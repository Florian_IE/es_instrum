#!/bin/bash

if [ -e /opt/bin/sitename.txt ]
then
    SITE=$(cat /opt/bin/sitename.txt)
else
    SITE="GNSS"
fi
IN_PATH="/dev/shm"
OUT_PATH="/opt/data/archive/gnss"
CONVBIN="/opt/bin/gps/convbin -ti 30.0 -tt 0.025 -ro -TADJ=1.0 -hc Time_tolerance=0.025s -v 3.04 -hm $SITE -ho M.Vidal/Geoazur -hr UNKNOWN/UBLOX-F9P/1.12 -ha UNKNOWN/AS-ANT2BCAL -d $OUT_PATH -o" 

mkdir -p $OUT_PATH
find $IN_PATH -name "*.ubx" -type f -mmin +1 |while read fname; do
  
  bname=${fname##*/}
  YYYYJJJ=$(date -d "${bname:5:8}" +%Y%j)
  OUT_FILE=$SITE"00FRA_R_"$YYYYJJJ${bname:13:4}"_01H_30S_MO.rnx"
  echo $CONVBIN $OUT_FILE $fname
  $($CONVBIN $OUT_FILE $fname 2>/dev/null)
  rm $fname
  gzip $OUT_PATH/$OUT_FILE
done
