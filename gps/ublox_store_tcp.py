#!/usr/bin/python2.7
import serial
import socket
from time import time, gmtime, strftime, sleep
from subprocess import Popen, PIPE, STDOUT, STDOUT
from sys import stderr
from os import path, getpid, getppid, devnull
from re import findall

#variables
debug=0
leapseconde=18
site="GNSS"
gnss_path="/dev/shm/"
tcp_port=5000
tcp_port2=5001
serial_port="/dev/ttyS1"

#check software is not currently in process
ps=Popen(["ps", "-ef"], stdout=PIPE)
ps_list=ps.communicate()[0]
for line in ps_list.splitlines():
        #if "ublox_store_tcp.py" in line and "python" in line and str(getpid()) not in line and str(getppid()) not in line:
        if "ublox_store_tcp.py" in line and "python" in line and str(getpid()) not in line:
            if debug:
                print line,"ublox_store_tcp.py already in process, Exiting..."
            exit()

#TCP NMEA server initialisation
server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server.bind(('', tcp_port))
server.setblocking(0)
server.listen(5)

#TCP UBX server initialisation
server2 = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server2.bind(('', tcp_port2))
server2.setblocking(0)
server2.listen(5)

#Serial default initialisation
ser = serial.Serial(serial_port,38400,parity=serial.PARITY_NONE,stopbits=serial.STOPBITS_ONE,bytesize=serial.EIGHTBITS,timeout=1,rtscts=False)
ser.flush()

#File logging function
def write(site, S, ext):
        date=strftime("_%Y%m%d%H00",gmtime(time()+leapseconde+1))
        filename=path.join(gnss_path,site+date+ext)
        #print "writing data in %s" % filename
        file=open(filename,'ab')
        file.write(S)
        file.close()

#extract NMEA messages from string
def get_nmea(S):
    n=''
    m = findall('\$G[NP]...,.*\r\n',S)
    if m:
        for nmea in m:
            S.replace(nmea,'')
            if ('TXT' not in nmea):
                n+=nmea
    return (n,S)

def configure_VTG(freq):
    #calculate UBX checksum
    Buffer=(0x06,0x01,0x03,0x00,0xf0,0x05,freq)
    CK_A = 0
    CK_B = 0
    for I in range(len(Buffer)):
        CK_A = CK_A + Buffer[I]
        CK_B = CK_B + CK_A
    Buffer=(0xB5,0x62)+Buffer+(CK_A,CK_B%256)
    ubx_trame= "".join("{:02x}".format(c) for c in Buffer)

    if debug>1:
        print ubx_trame
    if debug>0:
        print "# GNSS: %d cycle NMEA_VTG output request" % freq
    ser.write(ubx_trame.decode("hex"))

def configure_GSA(freq):
    #calculate UBX checksum
    Buffer=(0x06,0x01,0x03,0x00,0xf0,0x02,freq)
    CK_A = 0
    CK_B = 0
    for I in range(len(Buffer)):
        CK_A = CK_A + Buffer[I]
        CK_B = CK_B + CK_A
    Buffer=(0xB5,0x62)+Buffer+(CK_A,CK_B%256)
    ubx_trame= "".join("{:02x}".format(c) for c in Buffer)

    if debug>1:
        print ubx_trame
    if debug>0:
        print "# GNSS: %d cycle NMEA_GSA output request" % freq
    ser.write(ubx_trame.decode("hex"))

def configure_GSV(freq):
    #calculate UBX checksum
    Buffer=(0x06,0x01,0x03,0x00,0xf0,0x03,freq)
    CK_A = 0
    CK_B = 0
    for I in range(len(Buffer)):
        CK_A = CK_A + Buffer[I]
        CK_B = CK_B + CK_A
    Buffer=(0xB5,0x62)+Buffer+(CK_A,CK_B%256)
    ubx_trame= "".join("{:02x}".format(c) for c in Buffer)

    if debug>1:
        print ubx_trame
    if debug>0:
        print "# GNSS: %d cycle NMEA_GSV output request" % freq
    ser.write(ubx_trame.decode("hex"))

def configure_GLL(freq):
    #calculate UBX checksum
    Buffer=(0x06,0x01,0x03,0x00,0xf0,0x01,freq)
    CK_A = 0
    CK_B = 0
    for I in range(len(Buffer)):
        CK_A = CK_A + Buffer[I]
        CK_B = CK_B + CK_A
    Buffer=(0xB5,0x62)+Buffer+(CK_A,CK_B%256)
    ubx_trame= "".join("{:02x}".format(c) for c in Buffer)

    if debug>1:
        print ubx_trame
    if debug>0:
        print "# GNSS: %d cycle NMEA_GLL output request" % freq
    ser.write(ubx_trame.decode("hex"))

def configure_GGA(freq):
    #calculate UBX checksum
    Buffer=(0x06,0x01,0x03,0x00,0xf0,0x00,freq)
    CK_A = 0
    CK_B = 0
    for I in range(len(Buffer)):
        CK_A = CK_A + Buffer[I]
        CK_B = CK_B + CK_A
    Buffer=(0xB5,0x62)+Buffer+(CK_A,CK_B%256)
    ubx_trame= "".join("{:02x}".format(c) for c in Buffer)

    if debug>1:
        print ubx_trame
    if debug>0:
        print "# GNSS: %d cycle NMEA_GGA output request" % freq
    ser.write(ubx_trame.decode("hex"))


def configure_RAWX(freq):
    #calculate UBX checksum
    Buffer=(0x06,0x01,0x03,0x00,0x02,0x15,freq)
    CK_A = 0
    CK_B = 0
    for I in range(len(Buffer)):
        CK_A = CK_A + Buffer[I]
        CK_B = CK_B + CK_A
    Buffer=(0xB5,0x62)+Buffer+(CK_A,CK_B)
    ubx_trame= "".join("{:02x}".format(c) for c in Buffer)

    if debug>1:
        print ubx_trame
    if debug>0:
        print "# GNSS: %d cycle UBX-RAWX output request" % freq
    ser.write(ubx_trame.decode("hex"))

def configure_SFRBX(freq):
    #calculate UBX checksum
    Buffer=(0x06,0x01,0x03,0x00,0x02,0x13,freq)
    CK_A = 0
    CK_B = 0
    for I in range(len(Buffer)):
        CK_A = CK_A + Buffer[I]
        CK_B = CK_B + CK_A
    Buffer=(0xB5,0x62)+Buffer+(CK_A,CK_B)
    ubx_trame= "".join("{:02x}".format(c) for c in Buffer)

    if debug>1:
        print ubx_trame
    if debug>0:
        print "# GNSS: %d cycle UBX-SFRBX output request" % freq
    ser.write(ubx_trame.decode("hex"))

def configure_acq_rate(freq):
    if(freq==1):
        # obs rate=1000ms,nav cycle=1,time system=GPS
        # obs=1Hz, nav=1Hz
        #        CFG-PRT | 06 bits | obs rate| nav cycl| sys time
        Buffer=(0x06,0x08,0x06,0x00,0xe8,0x03,0x01,0x00,0x01,0x00)
        if debug>0:
            print "# GNSS: 1Hz acquisition request"
    elif(freq==5):
        # obs rate=200ms,nav cycle=1,time system=GPS
        # obs=5Hz, nav=5Hz
        #        CFG-PRT | 06 bits | obs rate| nav cycl| sys time
        Buffer=(0x06,0x08,0x06,0x00,0xc8,0x00,0x01,0x00,0x01,0x00)
        if debug>0:
            print "# GNSS: 5Hz acquisition request"
    elif(freq==20):
        # obs rate=50ms,nav cycle=20,time system=GPS
        # obs=20Hz, nav=1Hz
        #        CFG-PRT | 06 bits | obs rate| nav cycl| sys time
        Buffer=(0x06,0x08,0x06,0x00,0x32,0x00,0x14,0x00,0x01,0x00)
        if debug>0:
            print "# GNSS: 20Hz acquisition request"
    #calculate UBX checksum
    CK_A = 0
    CK_B = 0
    for I in range(len(Buffer)):
        CK_A = CK_A + Buffer[I]
        CK_B = CK_B + CK_A
    Buffer=(0xB5,0x62)+Buffer+(CK_A,CK_B%256)
    ubx_trame= "".join("{:02x}".format(c) for c in Buffer)

    if debug>1:
        print ubx_trame
    ser.write(ubx_trame.decode("hex"))

def configure_usb_proto():
    # in=RTCM3+UBX, out=UBX+NMEA
    #        CFG-PRT | 20 bits | USB|  NC|  TX pin | NC    NC   NC   NC   NC   NC   NC   NC| IN proto|OUT proto|  NC   NC   NC   NC
    Buffer=(0x06,0x00,0x14,0x00,0x03,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x33,0x00,0x03,0x00,0x00,0x00,0x00,0x00)
    #calculate UBX checksum
    CK_A = 0
    CK_B = 0
    for I in range(len(Buffer)):
        CK_A = CK_A + Buffer[I]
        CK_B = CK_B + CK_A
    Buffer=(0xB5,0x62)+Buffer+(CK_A,CK_B%256)
    ubx_trame= "".join("{:02x}".format(c) for c in Buffer)

    if debug>1:
        print ubx_trame
    if debug>0:
        print "# GNSS: USB protocole UBX request"
    ser.write(ubx_trame.decode("hex"))

def configure_uart_proto(serial_baud):
    if(serial_baud==9600):
        # in=RTCM3+UBX, out=NMEA
        # Bitfield=8N1
        # Baud=9600
        #        CFG-PRT | 20 bits | S2 |  NC|  TX pin | BitField Mode     | Baud              | IN proto|OUT proto|  NC   NC   NC   NC
        Buffer=(0x06,0x00,0x14,0x00,0x02,0x00,0x00,0x00,0xc0,0x08,0x00,0x00,0x80,0x25,0x00,0x00,0x33,0x00,0x02,0x00,0x00,0x00,0x00,0x00)
        if debug>0:
            print "# GNSS: UART at %dbaud with NMEA protocole request" % serial_baud
    elif(serial_baud==38400):
        # in=RTCM3+UBX, out=UBX+NMEA
        #        CFG-PRT | 20 bits | S2 |  NC|  TX pin | BitField Mode     | Baud              | IN proto|OUT proto|  NC   NC   NC   NC
        Buffer=(0x06,0x00,0x14,0x00,0x02,0x00,0x00,0x00,0xc0,0x08,0x00,0x00,0xc0,0x96,0x00,0x00,0x33,0x00,0x03,0x00,0x00,0x00,0x00,0x00)
        if debug>0:
            print "# GNSS: UART at %dbaud with UBX+NMEA protocole request" % serial_baud
    elif(serial_baud==115200):
        # in=RTCM3+UBX, out=UBX+NMEA
        #        CFG-PRT | 20 bits | S2 |  NC|  TX pin | BitField Mode     | Baud              | IN proto|OUT proto|  NC   NC   NC   NC
        Buffer=(0x06,0x00,0x14,0x00,0x02,0x00,0x00,0x00,0xc0,0x08,0x00,0x00,0x00,0xc2,0x01,0x00,0x33,0x00,0x03,0x00,0x00,0x00,0x00,0x00)
        if debug>0:
            print "# GNSS: UART at %dbaud with UBX+NMEA protocole request" % serial_baud
    elif(serial_baud==460800):
        # in=RTCM3+UBX, out=UBX+NMEA
        #        CFG-PRT | 20 bits | S2 |  NC|  TX pin | BitField Mode     | Baud              | IN proto|OUT proto|  NC   NC   NC   NC
        Buffer=(0x06,0x00,0x14,0x00,0x02,0x00,0x00,0x00,0xc0,0x08,0x00,0x00,0x00,0x07,0x08,0x00,0x33,0x00,0x03,0x00,0x00,0x00,0x00,0x00)
        if debug>0:
            print "# GNSS: UART at %dbaud with UBX+NMEA protocole request" % serial_baud
    else:
        print "# GNSS: UART %dbaud is not in the list (9600, 38400, 115200, 460800)\n\tPlease select the good serial speed\n" % serial_baud    
    #calculate UBX checksum
    CK_A = 0
    CK_B = 0
    for I in range(len(Buffer)):
        CK_A = CK_A + Buffer[I]
        CK_B = CK_B + CK_A
    Buffer=(0xB5,0x62)+Buffer+(CK_A%256,CK_B%256)
    ubx_trame= "".join("{:02x}".format(c) for c in Buffer)

    if debug>1:
        print ubx_trame
    ser.write(ubx_trame.decode("hex"))
    ser.baudrate=serial_baud
    ser.flush


########
# main #
########

#initialise receiver
if(serial_port=="/dev/ttyS1"):
    configure_uart_proto(38400)
else:
    configure_usb_proto()
sleep(0.1)
configure_acq_rate(20)
sleep(0.1)
configure_RAWX(1)
sleep(0.1)
configure_SFRBX(1)
sleep(0.1)
configure_GGA(0)
sleep(0.1)
configure_VTG(0)
sleep(0.1)
configure_GLL(0)
sleep(0.1)
configure_GSV(0)
sleep(0.1)
configure_GSA(0)

#initialise variables
buff=''
nmea=''
read_bits=0
address=("",0)
address2=("",0)


#main loop
while True:
        #accept TCP connection
        if not address[1]:
            try:
                client, address = server.accept()
                client.setblocking(0)
                if debug>0:
                    stderr.write("# NMEA server connection accepted from %s:%s on port %s\n" %(address[0],address[1],tcp_port))
            except:
                pass
        #accept TCP connection2
        if not address2[1]:
            try:
                client2, address2 = server2.accept()
                client2.setblocking(0)
                if debug>0:
                    stderr.write("# UBX server connection accepted from %s:%s on port %s\n" %(address2[0],address2[1],tcp_port2))
            except:
                pass

        #get serial queue
        sleep(0.001)
        bits=ser.inWaiting()
        #read serial and store data
        if bits:
            buff+=ser.read(bits)
            read_bits+=bits
        #when queue is empty
        else:
            #proceed data when not data readed 
            if buff:
                if debug>1:
                    stderr.write("read %d bits at %s" % (read_bits,strftime("%Y-%m-%d %H:%M:%S",gmtime())))
                #extract nmea messages
                (nmea,buff)=get_nmea(buff)
                if address[1]:
                    if debug>1 and nmea:
                        print nmea
                    #send nmea messages to tcp client
                    try:
                        if nmea:
                            client.send(nmea)
                    except:
                        if debug>0:
                            stderr.write("# NMEA server connection closed from %s:%s\n" %address)
                        client.close
                        address=("",0)
                if address2[1]:
                    #send raw observation to tcp client
                    try:
                        if buff:
                            client2.send(buff)
                    except:
                        if debug>0:
                            stderr.write("# UBX server connection closed from %s:%s\n" %address2)
                        client2.close
                        address2=("",0)
                #store raw data
                write(site,buff,'.ubx')
                #re-initialise buffer
                buff=''
                nmea=''
                read_bits=0
