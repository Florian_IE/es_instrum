#!/usr/bin/python2.7
import serial
from time import time, gmtime, strftime, sleep
from os import path, getpid, getppid, mkdir
from subprocess import Popen, PIPE


#variables
port="/dev/ttyS1"
baud=460800
gnss_path="/dev/shm"
leapseconde=18
gps_id=0
debug=0

#open serial port
ser = serial.Serial(port,baud,parity=serial.PARITY_NONE,stopbits=serial.STOPBITS_ONE,bytesize=serial.EIGHTBITS,timeout=1,rtscts=False)
ser.flush();

def reset():
    #calculate UBX checksum
    Buffer=(0x06,0x09,0x0d,0x00,0xff,0xfb,0x00,0x00,0x00,0x00,0x00,0x00,0xff,0xff,0x00,0x00,0x17)
    CK_A = 0
    CK_B = 0
    for I in range(len(Buffer)):
        CK_A = CK_A + Buffer[I]
        CK_B = CK_B + CK_A
    Buffer=(0xB5,0x62)+Buffer+(CK_A%256,CK_B%256)
    ubx_trame= "".join("{:02x}".format(c) for c in Buffer)

    if debug>1:
        print ubx_trame
    if debug>0:
        print "GNSS: reset receiver to defaul parameter"
    ser.write(ubx_trame.decode("hex"))
    ser.flush();


########
# main #
########

reset()
sleep(1)

