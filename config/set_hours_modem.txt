#Inscrire dans ce fichier les heures d'allumages et de coupure du modem
#Si rien inscrit, le modem sera allumé tout le temps
#
#Inscrire l'heure sour la forme HH:MM
#
#Exemple de syntaxe :
#
#on=12:00
#off=14:00
#on=18:00
#off=18:10
#
#on=10:00
#on=10:20
#off=10:10
#off=10:30
#
#La syntaxe et la casse est importante, merci de la respecter scrupuleusement

