#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 27 10:51:42 2023

@author: Florian Bourcier
CNRS/EOST
"""

import Adafruit_BBIO.GPIO as GPIO
import os
import time
import datetime

on = [] #Liste stockant les heures de démarrage du modem
off = [] #Liste stockant les heures de coupure du modem

count = 0 #Compteur de la réponse du modem au ping
status = 0 #variable définissant le status du modem :
#Status = 1 -> le modem est allumé
#Status = 2 -> Le modem est éteint

#Premier reboot du modem au démarrage du programme si le modem est éteint
response = os.system("ping -c 1 192.168.1.1")
if response != 0:
    status = 1
    GPIO.setup("P8_26", GPIO.OUT)
    GPIO.output("P8_26", GPIO.HIGH)
    time.sleep(30)
    GPIO.setup("P8_26", GPIO.LOW)
    time.sleep(300)
else:
    status = 1


#Traitement des heures d'allumages et de coupure du fichier texte
with open("/opt/bin/config/set_hours_modem.txt", "r") as fichier:
    ligne_fichier = fichier.readlines()
    for i in ligne_fichier:
        if i[0] == "#":
            pass
        elif i[0:2] == "on":
            on.append(i[3:8])
        elif i[0:3] == "off":
            off.append(i[4:9])

def reboot(status): #Fonction de reboot du modem et de mis à jour du fichier de config des heures
    GPIO.setup("P8_26", GPIO.OUT)
    GPIO.output("P8_26", GPIO.HIGH)
    time.sleep(30)
    GPIO.output("P8_26", GPIO.LOW)
    time.sleep(300)
    return 0
    
def shutdown(): #Eteint le modem
    GPIO.setup("P8_26", GPIO.OUT)
    GPIO.output("P8_26", GPIO.HIGH)
    time.sleep(30)
    return 0

def power(): #Allume le modem
    GPIO.setup("P8_26", GPIO.OUT)
    GPIO.output("P8_26", GPIO.LOW)
    time.sleep(300)
    return 0

def read(): #Lecture du fichier de config des heures
    on = []
    off = []
    with open("/opt/bin/config/set_hours_modem.txt", "r") as fichier:
        ligne_fichier = fichier.readlines()
        for i in ligne_fichier:
            if i[0] == "#":
                pass
            elif i[0:2] == "on":
                on.append(i[3:8])
            elif i[0:3] == "off":
                off.append(i[4:9])    
    
#Récupération de l'heure courante
time_now = datetime.datetime.today().strftime('%H:%M')

while True:
    
    time_now = datetime.datetime.today().strftime('%H:%M')
    
    #Allumage du modem en fonction de l'heure
    for i in range(len(on)):
        if time_now == on[i]:
            status = 1
            count = 0
            os.system("echo '**********'")
            os.system("echo 'Wake up time ! Modem on !'")
            os.system("echo '**********'")
            power()
    
    #Coupure du modem en fonction de l'heure
    for i in range(len(off)):
        if time_now == off[i]:
            status = 2
            os.system("echo '**********'")
            os.system("echo 'Off time, cut the modem'")
            os.system("echo '**********'")
            shutdown()
    
    #Reboot tous les jours à minuit
    if time_now == "00:00":
        read()
        if status == 1:
            os.system("echo '**********'")
            os.system("echo 'Midnight ! Reboot of the modem !'")
            os.system("echo '**********'")
            count = 0
            reboot()
    
    #Conditions pour savoir si on doit pinger le modem ou non
    if status == 1:
        #Ping du modem
        response = os.system("ping -c 1 192.168.1.1")        
        
        if response != 0:
            count+=1
        else:
            count = 0
            
        if count > 60:
            count = 0
            os.system("echo '**********'")
            os.system("echo 'Modem not responding : reboot !'")
            os.system("echo '**********'")
            reboot()

    time.sleep(20)