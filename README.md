# ES_Instrum
##Description

Es_Instrum est une architecture de programmes ayant pour but d'être installé sur les stations Envirosciences-Pyrénées pour effectuer l'acquisition des différents capteurs.
Cette architecture viens avec le Dépot ES_Hardware, qui est le dépot contenant les fichiers de fabrication des cartes dévellopé pour le projet.

Ce dépot se compose de 2 branches :
 - La branche main contien les programmes d'acquisitions.
 - La branch BB_set qui contient l'image Debian qui correspond aux programmes.




##Installation

Pour installer cette architecture, vous devez suivre ces instructions :
 - Changer les droit d'accès au répertoir /opt et créer un dossier /opt/bin/ dans la Beaglebone. "sudo chmod 666 /opt && mkdir /opt/bin" 
 - Copier les fichiers de la branche main dans /opt/bin/
 - Lancer le programme "sudo /opt/bin/install.sh" et suivre les indications
 - Redémarrer la Beaglebone

## Auteurs et Licence

Auteurs :
 - Florian Bourcier
 - Maurin Vidal
 - Nicolas Chatelain
 - Hélène Jund
 - Maxime Bes de Berc

Ce dépot est en licence libre. Inch Allah si le CNRS le veut bien.

## Status du projet

Ce projet est encore en cours de dévelloppement et d'amélioration par l'équipe instrumentation de l'EOST/OMIV.
